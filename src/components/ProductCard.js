import { Card, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function ProductCard({productProp}) {


    const { _id, name, description, price } = productProp;


    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>
                    {name}
                </Card.Title>
                <Card.Subtitle>
                    Description:
                </Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>

                <Button as={Link} to={`/products/${_id}`} variant="primary">Details</Button>
            </Card.Body>
        </Card>
    )
}

