import Banner from "../components/Banner";
import Highlights from "../components/Highlights";


export default function Home(){
	const data = {
		title: "WELCOME TO WIN SHOP!",
		content: "“Where the best deals are just one click away”",
		destination: "/login",
		label: "Shop now!"
	}
	return(
		<>

			<Banner bannerProp={data}/>
			<Highlights />

		</>
	)
}